using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RotateCube : MonoBehaviour
{

    [SerializeField] Vector3 Rot;
    public float speed = 3.0f;
    public InputAction activateRotation;
    public bool turn = false;

    // Start is called before the first frame update
    void Start()
    {
        activateRotation.Enable();
        activateRotation.started += ctx => turn = !turn;
    }

    // Update is called once per frame
    void Update()
    {
        if (turn)
        {
          transform.Rotate(speed * Time.deltaTime * Rot);
       }
    }
}
