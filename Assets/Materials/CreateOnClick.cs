using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CreateOnClick : MonoBehaviour
{
    public GameObject aCopier;
    public GameObject obj;
    public Color Couleur;
    public InputAction CreerObject;
    public bool newObj = false;

    
    // Start is called before the first frame update
    void Start()
    {
        CreerObject.Enable();
        CreerObject.started += ctx => newObj = !newObj;
       //git init --initial-branch=main
    }

    // Update is called once per frame
    void Update()
    {
        if (newObj)
        {
            obj = new GameObject();
            obj.GetComponent<Renderer>().material.color = Color.yellow;
            obj.transform.position = Camera.main.transform.forward + new Vector3(0, 0, 2);
        }
    }
}
