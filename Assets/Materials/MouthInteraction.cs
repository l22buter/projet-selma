using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class MouthInteraction : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler

{

    // Attributs

    public Renderer ren;
    public Rigidbody rb;
    public int force = 1000;
    public Color InitialColor;

    // Fonctions


    // Start is called before the first frame update
void Start()
    {
        ren = GetComponent<Renderer>();
        rb = GetComponent<Rigidbody>();
        InitialColor = ren.material.color;
    }


public void OnPointerClick(PointerEventData pointerEventData)
    {
        rb.AddForce(Camera.main.transform.forward * force);
    }


public void OnPointerEnter(PointerEventData pointerEventData) // Quand la souris passe sur l'objet
    {
        ren.material.color = Color.red;
        Debug.Log("Enter");
    }


public void OnPointerExit(PointerEventData pointerEventData) // Quand la souris n'est plus sur l'objet
    {
        ren.material.color = InitialColor;
    }


    // Update is called once per frame
    void Update()
    {
    }


}
